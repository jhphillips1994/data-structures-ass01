import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

// TODO: Test with all different inputs including strings in the menu system

public class Assignment01 {
	// Setup scanner
	static Scanner scan = new Scanner(System.in);
	
	/** 
	 * Setup and run the menu selection and menu system
	 * 
	 * @param inputLists Array of linkedLists will be used by the application
	 */
	public static void menuSystem(SingleDigitIntegerLinkedList[] inputLists) {
		int menuSelection = getMenuSelection();
		runMenuSystem(menuSelection, inputLists);
	}
	
	/**
	 * Display the menu to the screen and prompt the user for input
	 * 
	 * @return Return the number of the menu option the user selected
	 * 
	 */
	public static int getMenuSelection() {
		int menuSelection = 0;
		while(menuSelection < 1 ||  menuSelection > 4) {
			menuSelection = 1;
			System.out.println("\n=========== Large integer addition ==========");
			System.out.println("| Enter 1 to Read numbers from File         |");
			System.out.println("| Enter 2 to Display integers stored in SILL|");
			System.out.println("| Enter 3 to Display addition result        |");
			System.out.println("| Enter 4 to Exit                           |");
			System.out.println("=============================================");
			try {
				menuSelection = scan.nextInt();
			} catch(InputMismatchException e) {
				System.out.println("\nError: A String was entered");
				scan.nextLine();
				getMenuSelection();
			}
			if(menuSelection < 1 ||  menuSelection > 4) {
				System.out.println("\nInvalid Option: Please enter a correct option (1~4) as listed above\n");
			}
		}
		return menuSelection;
	}
	
	/**
	 * Determines what to do depending on what menu option was selected
	 */
	public static void runMenuSystem(int menuSelection, SingleDigitIntegerLinkedList[] inputLists) {
		// Menu function calls
		switch (menuSelection) {
		case 1:
			readNumbersFromFile(inputLists);
			break;
		case 2:
			displayIntegersStoredinSill(inputLists, menuSelection);
			break;
		case 3:
			SingleDigitIntegerLinkedList result = reverseAndAddLinkedLists(inputLists[0], inputLists[1]);
			// Printing the final result
			printAdditionResults(inputLists[0], inputLists[1], result);
			menuSystem(inputLists);
			break;
		case 4:
			System.exit(0);
		default:
			System.exit(0);
		}
	}
	
	/**
	 * Manages the Read Numbers From File option in the menu. Calls the text file read methods
	 * 
	 * @param inputLists is the array of linkedLists that will store the values retrieved from the file
	 */
	public static void readNumbersFromFile(SingleDigitIntegerLinkedList[] inputLists) {
		System.out.println("Please enter file name:");
		while(true) {
			// The name of the file that will be opened
			String inputFile = scan.next();
			// Method to read file
			try {
				inputLists = readTextFile(inputFile);
			} catch (FileNotFoundException e) {
				System.out.println("File read failed: Couldn�t find " + inputFile +".txt file! Please check the file name and enter again");
				continue;
			}
			System.out.println("\nFound the " + inputFile + ".txt file and read the numbers successfully");
			menuSystem(inputLists);
		}
	}
	
	/**
	 * Reads the text file and returns a linkedList of the two values from the text file
	 * 
	 * @param file The name of the text file
	 * 
	 * @return A linkedList containing the two values pulled from the inputed file 
	 * 
	 * @throws FileNotFoundException If the inputed file is not found
	 */
	public static SingleDigitIntegerLinkedList[] readTextFile(String file) throws FileNotFoundException {
		file = file + ".txt";
		SingleDigitIntegerLinkedList[] inputLists;
		try {
			// Reading the file with default encoding
			FileReader fileReader = new FileReader(file);
			// Wrapping the fileReader in a BUfferedReader
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			inputLists = readTextLinesFromBuffer(bufferedReader, file);
		} catch(FileNotFoundException e){
			throw new FileNotFoundException();
		}
		return inputLists;
	}
	
	/**
	 * Reads each of the lines within the inputed bufferedReader
	 * 
	 * @param bufferedReader that the lines will be read from
	 * 
	 * @param file the name of the file to be displayed if there is an error
	 * 
	 * @return an array of linkedLists with the data read from the text file
	 */
	public static SingleDigitIntegerLinkedList[] readTextLinesFromBuffer(BufferedReader bufferedReader,
			String file) {
		// Setting up file line reference and line counter
		String line;
		int lineCounter = 0;
		SingleDigitIntegerLinkedList[] inputLists = new SingleDigitIntegerLinkedList[2];

		try {
			// Reading the file line by line
			while((line = bufferedReader.readLine()) != null){
				String[] characters = new String[line.length()];
				characters = line.split("");
				SingleDigitIntegerLinkedList integerLinkedList;
				if(characters[0].equals("-")) {
					// Setup linkedLists with boolean parameter to indicate linkedList is negative
					integerLinkedList = new SingleDigitIntegerLinkedList(true);
				} else {
					// Setup linkedLists with no parameter to indicate the linkedList is positive
					integerLinkedList = new SingleDigitIntegerLinkedList();
				}
				addTextLineToLinkedList(integerLinkedList, characters);
				inputLists[lineCounter++] = integerLinkedList;
			}
			// Closing the file
			bufferedReader.close();
		} catch(IOException e){
			System.out.println("Error reading the file: " + file);
		}
		return inputLists;
	}
	
	/**
	 * Cycles through the inputed character array and adds each integer into the inputed linkList
	 * The first character is checked to determine if the value is positive or negative. If the value is negative, the negative flag will be set on
	 * the linkedList
	 * 
	 * @param integerLinkedList THe linkedList that all the integers will be added to.
	 * 
	 * @param characters The array of characters that will be added to the linkedList.
	 */
	public static void addTextLineToLinkedList(SingleDigitIntegerLinkedList integerLinkedList, String[] characters) {
		for(String character : characters) {
			// If the first character is the minus symbol skip it
			if(character.equals("-")) {
				continue;
			}
			try {
				// Throws an error when a character is included within the input file
				integerLinkedList.addNode(Integer.parseInt(character));
			} catch(NumberFormatException e) {
				System.out.println("The file contains a non numeric character\n");
				throw new NumberFormatException();
			}
		}
	}
	
	/**
	 * Reverse and add the two inputed linkedLists together
	 * 
	 * @param listOne First linkedList to be added
	 * 
	 * @param listTwo Second linkedList to be added
	 * 
	 * @return LinkedList with the resulting value from the sum of the two inputed linkedLists
	 */
	public static SingleDigitIntegerLinkedList reverseAndAddLinkedLists(SingleDigitIntegerLinkedList listOne, SingleDigitIntegerLinkedList listTwo) {
		// Cloning the linkedLists so they can be changed
		SingleDigitIntegerLinkedList tempListOne = listOne.clone();
		SingleDigitIntegerLinkedList tempListTwo = listTwo.clone();
		
		// Reversing the linked lists to so the addition calculation is simple
		SingleDigitIntegerLinkedListNode firstReverseLinkedList = reverseLinkedList(tempListOne.head);
		SingleDigitIntegerLinkedListNode secondReverseLinkedList = reverseLinkedList(tempListTwo.head);		
		
		// Adding the two reversed lists together
		SingleDigitIntegerLinkedList result = addLinkedLists(firstReverseLinkedList, secondReverseLinkedList, listOne.negativeValue);
		return result;
	}
	
	/**
	 * Reverses the LinkedList
	 * 
	 * @param head The head of the linkedList that will be reversed.
	 * 
	 * @return The head of the reversed linkedList
	 */
	public static SingleDigitIntegerLinkedListNode reverseLinkedList(SingleDigitIntegerLinkedListNode head) {
		SingleDigitIntegerLinkedListNode current = head;
		SingleDigitIntegerLinkedListNode next = null;
		SingleDigitIntegerLinkedListNode previous = null;
		
		while(current != null) {
			next = current.nextLink;
			current.nextLink = previous;
			previous = current;
			current = next;
		}
		head = previous;
		return head;
	}
	
	/**
	 * Adds the two inputed linkedLists together and returns them as a single linkedList
	 * 
	 * @param listOne First linkedList to be added
	 * 
	 * @param listTwo Second linkedList to be added
	 * 
	 * @param negativeValue Flag to determine if the resulting value will be negative
	 *  
	 * @return A linkedList with the result of the sum of the two inputed linkedLists
	 */
	public static SingleDigitIntegerLinkedList addLinkedLists(SingleDigitIntegerLinkedListNode listOne, 
			SingleDigitIntegerLinkedListNode listTwo,
			boolean negativeValue) {
		int carry = 0;
		int sum = 0;
		int iteration = 0;
		SingleDigitIntegerLinkedList result = new SingleDigitIntegerLinkedList(negativeValue);
		
		while(listOne != null || listTwo != null) {
			iteration++;
			sum = carry;
			if(listOne != null) {
				sum = sum + listOne.data;
				listOne = listOne.nextLink;
			}
			if(listTwo != null) {
				sum = sum + listTwo.data;
				listTwo = listTwo.nextLink;
			}
			carry = sum / 10;
			sum = sum % 10;
			if(iteration == 1) {
				result.addNode(sum);
			} else {
				result.addNode(sum);
			}
		}
		if(carry != 0) {
			result.addNode(carry);
		}
		// Reversing the result list
		result.head = reverseLinkedList(result.head);
		return result;
	}
	
	/**
	 * Handles the Display Integers Stored in SILL option on the menu. Checks if the file has been loaded and
	 * displays the linkedLists
	 * 
	 * @param inputLists is the array of LinkedLists
	 * 
	 * @param menuSelection the number of the menu option that was selected
	 */
	public static void displayIntegersStoredinSill(SingleDigitIntegerLinkedList[] inputLists, int menuSelection) {
		if(inputLists == null) {
			System.out.println("\nFile is not loaded yet. Please first read numbers using option 1");
			menuSelection = scan.nextInt();
			runMenuSystem(menuSelection, inputLists);
		} else {
			displayLinkedLists(inputLists);
		}
		menuSystem(inputLists);
	}
	
	/** 
	 * Cycle through an array of linkedLists to print each of them. It will also print the amount of nodes in the list as well
	 * 
	 * @param inputLists List of linkedLists to be printed
	 */
	public static void displayLinkedLists(SingleDigitIntegerLinkedList[] inputLists) {
		for(SingleDigitIntegerLinkedList list : inputLists) {
			list.printLinkedList();
			int nodeCount = list.getNodeCount();
			System.out.print(" Number of Nodes: " + nodeCount + "\n");
		}
	}
	
	/**
	 * Prints the first two linkedLists and the result linkedList in the correct format
	 * 
	 * @param listOne First list of the addition, this is required to display the values
	 * 
	 * @param listTwo Second list of the addition, this is required to display the values
	 * 
	 * @param result LinkedList of the result values from the sum of the first two inputed linkedLists
	 */
	public static void printAdditionResults(SingleDigitIntegerLinkedList listOne, 
			SingleDigitIntegerLinkedList listTwo, 
			SingleDigitIntegerLinkedList result) {
		listOne.printLinkedList();
		if(listOne.negativeValue) {
			System.out.print(" - ");
		} else {
			System.out.print(" + ");	
		}
		// Flicking the negative boolean to remove the extra - symbol when printing to the screen
		listTwo.negativeValue = false;
		listTwo.printLinkedList();
		listTwo.negativeValue = true;
		System.out.print(" = ");
		result.printLinkedList();
		int nodeCount = result.getNodeCount();
		System.out.println("\n\nNumber of nodes used to store result: " + nodeCount);
	}
	
	/**
	 * Main process function
	 * 
	 * @param args
	 */
	public static void main(String[] args){		
		// Start of the menu system
		SingleDigitIntegerLinkedList[] inputLists = null;
		menuSystem(inputLists);
		
	}
}

