/**
 * The SingleDigitIntegerLinkedLisk class is made up of these nodes connected together.
 * Each node contains data and a link to the next node in the list.
 * 
 * @author Jack Phillips
 *
 */
public class SingleDigitIntegerLinkedListNode {
	public int data;
	public SingleDigitIntegerLinkedListNode nextLink;
	
	/**
	 * Constructor method
	 * 
	 * @param newNodeData The integer data that will be stored within the Linked List Node
	 */
	public SingleDigitIntegerLinkedListNode(int newNodeData) {
		this.data = newNodeData;
		this.nextLink = null;
	}
}
