/**
 * A list of nodes each containing a link to next node.
 * The list is linear way to store data.
 * 
 * @author Jack Phillips
 */
public class SingleDigitIntegerLinkedList {
	public SingleDigitIntegerLinkedListNode head;
	public boolean negativeValue;
	
	/**
	 * Constructor Method without a boolean to indicate that the integer array
	 * 
	 */
	public SingleDigitIntegerLinkedList() {
		this.head = null;
		this.negativeValue = false;
	}
	
	/**
	 *  Class constructor Method when a boolean is passed as a parameter to indicate that the integer linked list array is negative
	 *  
	 *  @param negativeInt boolean to specify if the linkedList contains a negative value
	 */
	public SingleDigitIntegerLinkedList(boolean negativeInt) {
		this.head = null;
		if(negativeInt) {
			this.negativeValue = true;
		} else {
			this.negativeValue = false;
		}
	}
	
	/** 
	 * Counts the amount of nodes in this linked list
	 *  
	 * @return the number of nodes within the linkedList
	 */
	public int getNodeCount() {
		SingleDigitIntegerLinkedListNode temp = this.head;
		int counter = 0;
		while(temp != null) {
			counter++;
			temp = temp.nextLink;
		}
		return counter;
	}
	
	/**
	 * Method to add a node to the end of the linked list
	 * 
	 * @param newNodeData an integer that will be contained within the new node that will be added to the end of the linked list
	 */
	public void addNode(int newNodeData) {
		SingleDigitIntegerLinkedListNode newNode = new SingleDigitIntegerLinkedListNode(newNodeData);
		SingleDigitIntegerLinkedListNode temp = head;
		
		if(head == null) {
			head = newNode;
		} else {
			while(temp.nextLink != null) {
				temp = temp.nextLink;
			}
			temp.nextLink = newNode;
		}
	}
	
	/**
	 * Method to print all of the values within the linkedList, if the linked list has the negative flag
	 * it will be prefixed with a - symbol
	 */
	public void printLinkedList() {
		SingleDigitIntegerLinkedListNode temp = this.head;
		
		// If the value is negative a minus symbol is displayed at the front
		if(this.negativeValue) {
			System.out.print("-");
		}
		while(temp != null) {
			System.out.print(temp.data);
			temp = temp.nextLink;
		}
	}
	
	/**
	 * Clones the current linkedList and returns a copy of it
	 * 
	 * @return A copy of this linkedList
	 */
	public SingleDigitIntegerLinkedList clone() {
		SingleDigitIntegerLinkedListNode temp = this.head;
		SingleDigitIntegerLinkedList newList = new SingleDigitIntegerLinkedList(this.negativeValue);
		while(temp != null) {
			newList.addNode(temp.data);
			temp = temp.nextLink;
		}
		return newList;
	}
}